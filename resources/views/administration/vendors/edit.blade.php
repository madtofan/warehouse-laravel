@extends('navbar')
@section('context')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update Vendor</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('vendors.update', $vendor->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="vendor_name">Name:</label>
                <input type="text" class="form-control" name="vendor_name" value={{ $vendor->vendor_name }} />
            </div>

            <div class="form-group">
                <label for="vendor_address">Address:</label>
                <input type="text" class="form-control" name="vendor_address" value={{ $vendor->vendor_address }} />
            </div>

            <div class="form-group">
                <label for="vendor_email">Email:</label>
                <input type="email" class="form-control" name="vendor_email" value={{ $vendor->vendor_email }} />
            </div>
            <div class="form-group">
                <label for="vendor_number">Number:</label>
                <input type="tel" class="form-control" name="vendor_number" pattern="[0-9]{3}-[0-9]{7}" value={{ $vendor->vendor_number }} />
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
              <button type="reset" value="reset">Clear Item</button>
        </form>
    </div>
</div>
@endsection
