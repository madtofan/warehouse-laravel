@extends('navbar') @section('context')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                    <h3>Add Vendor List</h3>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br /> @endif
                        <form method="post" action="{{ route('vendors.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="vendor_name" class="col-md-3 col-form-label text-md-right">Name</label>
                                <div class="col-md-8">
                                    <input id="vendor_name" type="text" class="form-control" name="vendor_name" required autofocus/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="vendor_address" class="col-md-3 col-form-label text-md-right">Address</label>
                                <div class="col-md-8">
                                    <input id="vendor_address" type="text" class="form-control" name="vendor_address" required autofocus/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="vendor_email" class="col-md-3 col-form-label text-md-right">Email</label>
                                <div class="col-md-8">
                                    <input id="vendor_email" type="email" class="form-control" name="vendor_email" required autofocus/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="vendor_number" class="col-md-3 col-form-label text-md-right">Phone Number</label>
                                <div class="col-md-8">
                                    <input id="vendor_number" type="tel" class="form-control" name="vendor_number" pattern="[0-9]{3}-[0-9]{7}" text="sample 012-3456789" required autofocus/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Add Item</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
@endsection
