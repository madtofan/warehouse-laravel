@extends('navbar') @section('context')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <h1 class="display-3"><img src="/images/icons/vendors.png" class="page-logo"/>Vendors Management</h1> @if(auth()->user()->role->admin == 1)
                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                    <a href="{{ route('vendors.create')}}" class="btn btn-primary">Register New Vendor</a>
                  </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <a class="btn download" href='/download/vendortemplatecsv.csv'> Download Template </a>
                    </div>
                </div>
                <form method='post' action='/uploadvendorfile' enctype='multipart/form-data'>
                    {{ csrf_field() }}
                    <label for="file-upload" class="custom-file-upload">
                        <i class="fa fa-cloud-upload"></i> File Upload
                    </label>
                    <input id="file-upload" type="file" name='file' />

                    <input type='submit' name='submit' class="btn btn-primary" value='Import'>
                </form>
              </div>
            </div>
                @endif
                <div class="form-group row">
                    <div class="col-md-8">
                        <label for="search" class="col-md-3 col-form-label text-md-right">Search vendor</label>
                        <input id="search" autocomplete="off" type="text" class="typeahead form-control" name="search_value" value="{{ $filter_value }}">
                        <script>
                            $('.typeahead').on('keydown', function(e) {
                                if (e.keyCode == 13) {
                                    var search_text = document.getElementById("search").value;
                                    window.location.replace("/vendors/filter/".concat(search_text));
                                }
                            });
                        </script>
                    </div>
                </div>
                <div style="overflow-x:auto;">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>@sortablelink('vendor_name', 'Company Name')</th>
                                <th>@sortablelink('vendor_address', 'Company Address')</th>
                                <th>@sortablelink('vendor_email', 'Company Email')</th>
                                <th>@sortablelink('vendor_number', 'Company Number')</th>
                                <th colspan=3>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($vendors as $vendor)
                            <tr>
                                <td>{{$vendor->vendor_name}}</td>
                                <td>{{$vendor->vendor_address}}</td>
                                <td>{{$vendor->vendor_email}}</td>
                                <td>{{$vendor->vendor_number}}</td>
                                @if(auth()->user()->role->admin == 1)
                                <td>
                                    <a href="/contacts/{{$vendor->id}}" class="btn btn-primary">Contacts</a>
                                </td>
                                <td>
                                    <a href="{{ route('vendors.edit',$vendor->id)}}" class="btn btn-primary">Edit</a>
                                </td>
                                <td>
                                    <form class="delete" action="{{ route('vendors.destroy', $vendor->id)}}" method="post">
                                        @csrf @method('DELETE')
                                        <input type="submit" class="btn btn-danger" value="Delete">
                                    </form>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $vendors->appends(\Request::except('page'))->render() !!}
            </div>
        </div>

        <script>
            $(".delete").on("submit", function(){
                return confirm("Are you sure?");
            });
        </script>
@endsection
