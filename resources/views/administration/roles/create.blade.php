@extends('navbar')

@section('context')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add Roles list</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('roles.store') }}">
          @csrf
          <div class="form-group">
              <label for="role_name">Name:</label>
              <input type="text" class="form-control" name="role_name"/>
          </div>

          <div class="form-group">
              <label for="admin">Admin:</label>
              <input type="radio" class="option-input radio" name="admin" id="1" value="1">&emsp; Authorize &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" class="option-input radio" name="admin" id="0" value="0">&emsp; Restrict
          </div>
<br><br>
          <div class="form-group">
              <label for="moderator">Moderator:</label>
              <input type="radio" class="option-input radio" name="moderator" id="1" value="1">&emsp; Authorize &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" class="option-input radio" name="moderator" id="0" value="0">&emsp; Restrict
          </div>
<br><br>
          <div class="form-group">
              <label for="receiving">Receiving:</label>
              <input type="radio" class="option-input radio" name="receiving" id="1" value="1">&emsp; Authorize &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" class="option-input radio" name="receiving" id="0" value="0">&emsp; Restrict
          </div>
<br><br>
          <div class="form-group">
              <label for="inventory">Inventory:</label>
              <input type="radio" class="option-input radio" name="inventory" id="1" value="1">&emsp; Authorize &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" class="option-input radio" name="inventory" id="0" value="0">&emsp; Restrict
          </div>
<br><br>
          <div class="form-group">
              <label for="putting_away">Putting Away:</label>
              <input type="radio" class="option-input radio" name="putting_away" id="1" value="1">&emsp; Authorize &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" class="option-input radio" name="putting_away" id="0" value="0">&emsp; Restrict
          </div>
<br><br>
<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
          <button type="submit" class="btn btn-primary">Add Item</button>
        </div>
      </div>
      </form>
  </div>
</div>

@endsection
