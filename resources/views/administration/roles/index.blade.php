@extends('navbar') @section('context')
<div class="col-sm-12">

    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
</div>
<div class="row">
  <div class="col-sm-12">
        <h1 class="display-3"><img class="page-logo" src="/images/icons/roles.png"/>Roles Management</h1>
        <div>
            <a style="margin: 19px;" href="{{ route('roles.create')}}" class="btn btn-primary">Register New Role</a>
        </div>
        <div class="form-group row">
            <div class="col-md-8">
                <label for="search" class="col-md-3 col-form-label text-md-right">Search roles</label>
                <input id="search" autocomplete="off" type="text" class="typeahead form-control" name="search_value" value="{{ $filter_value }}">
                <script>
                    $('.typeahead').on('keydown', function(e) {
                        if (e.keyCode == 13) {
                            var search_text = document.getElementById("search").value;
                            window.location.replace("/roles/filter/".concat(search_text));
                        }
                    });
                </script>
            </div>
        </div>
        <div style="overflow-x:auto;">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>@sortablelink('role_name', 'Role Name')</th>
                        <th>@sortablelink('admin', 'Admin')</th>
                        <th>@sortablelink('moderator', 'Moderator')</th>
                        <th>@sortablelink('receiving', 'Receiving')</th>
                        <th>@sortablelink('inventory', 'Inventory')</th>
                        <th>@sortablelink('putting_away', 'Putting Away')</th>
                        <th colspan=2>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $role)
                    <tr>
                        <td>{{$role->role_name}}</td>
                        <td>@if($role->admin == 1) Authorized @else Restricted @endif
                        </td>
                        <td>@if($role->moderator == 1) Authorized @else Restricted @endif
                        </td>
                        <td>@if($role->receiving == 1) Authorized @else Restricted @endif
                        </td>
                        <td>@if($role->inventory == 1) Authorized @else Restricted @endif
                        </td>
                        <td>@if($role->putting_away == 1) Authorized @else Restricted @endif
                        </td>
                        <td>
                            <a href="{{ route('roles.edit',$role->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form class="delete" action="{{ route('roles.destroy', $role->id)}}" method="post">
                                @csrf @method('DELETE')
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {!! $roles->appends(\Request::except('page'))->render() !!}
    </div>
</div>

<script>
    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>

@endsection
