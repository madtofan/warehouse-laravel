@extends('navbar') @section('context')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update role list</h1> @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> @endif
        <form method="post" action="{{ route('roles.update', $role->id) }}">
            @method('PATCH') @csrf
            <div class="form-group">

                <label for="role_name">Name:</label>
                <input type="text" class="form-control" name="role_name" value="{{ $role->role_name }}" />
            </div>

            <div class="form-group">
                <label for="admin">Admin:</label>
                <input type="radio" class="option-input radio" name="admin" value="1" id="1" />&emsp; Authorize &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" class="option-input radio" name="admin" value="0" id="0" />&emsp; Restrict
            </div>

            <div class="form-group">
                <label for="moderator">Moderator:</label>
                <input type="radio" class="option-input radio" name="moderator" value="1" id="1" />&emsp;Authorize &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" class="option-input radio" name="moderator" value="0" id="0" />&emsp;Restrict
            </div>

            <div class="form-group">
                <label for="receiving">Receiving:</label>
                <input type="radio" class="option-input radio" name="receiving" value="1" id="1" />&emsp;Authorize &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" class="option-input radio" name="receiving" value="0" id="0" />&emsp;Restrict
            </div>

            <div class="form-group">
                <label for="inventory">Inventory:</label>
                <input type="radio" class="option-input radio" name="inventory" value="1" id="1" />&emsp;Authorize &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" class="option-input radio" name="inventory" value="0" id="0" />&emsp;Restrict
            </div>

            <div class="form-group">
                <label for="putting_away">Putting Away:</label>
                <input type="radio" class="option-input radio" name="putting_away" value="1" id="1" />&emsp;Authorize &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" class="option-input radio" name="putting_away" value="0" id="0" />&emsp;Restrict
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
            <button type="reset" value="reset">Clear Item</button>
        </form>
    </div>
</div>
@endsection
