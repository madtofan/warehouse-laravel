@extends('navbar') @section('context')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                    <h1>Add Contact</h1>

                <div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br /> @endif
                    <div class="card-body">
                        <form method="post" action="{{ route('contacts.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">Name</label>
                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="name" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="vendors_id" class="col-md-3 col-form-label text-md-right">Company Name</label>
                                <div class="col-md-8">
                                    <select id="vendors_id" type="text" class="form-control selectpicker" name="vendors_id" title="Select vendor">
                                        @foreach ($vendorDD_array as $data)
                                        <option value="{{ $data->id }}">{{ $data->vendor_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-3 col-form-label text-md-right">Email</label>
                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control" name="email" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="job_title" class="col-md-3 col-form-label text-md-right">Position</label>
                                <div class="col-md-8">
                                    <input id="job_title" type="text" class="form-control" name="job_title" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-3 col-form-label text-md-right">City</label>
                                <div class="col-md-8">
                                    <input id="city" type="text" class="form-control" name="city" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone_number" class="col-md-3 col-form-label text-md-right">Phone Number:</label>
                                <div class="col-md-8">
                                    <input id="phone_number" type="tel" class="form-control" name="phone_number" pattern="[0-9]{3}-[0-9]{7}" title="sample 012-3456789" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Add Contact</button>
                                </div>
                            </div>
                        </form>
                    </div>
                  </div>
                </div>
    </div>
</div>
@endsection
