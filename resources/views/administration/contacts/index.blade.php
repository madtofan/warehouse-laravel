@extends('navbar') @section('context')
<div class="col-sm-12">

    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
</div>
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3"><img src="/images/icons/contacts.png" class="page-logo"/>Contacts Management</h1> @if(auth()->user()->role->admin == 1)
        <div>
            <a style="margin: 19px;" href="{{ route('contacts.create')}}" class="btn btn-primary">Register New Contact</a>
        </div>
        @endif
        <div class="form-group row">
            <div class="col-md-8">
                <label for="search" class="col-md-3 col-form-label text-md-right">Search contact</label>
                <input id="search" autocomplete="off" type="text" class="typeahead form-control" name="search_value" value="{{ $filter_value }}">
                <script>
                    $('.typeahead').on('keydown', function(e) {
                        if (e.keyCode == 13) {
                            var search_text = document.getElementById("search").value;
                            window.location.replace("/contacts/filter/".concat(search_text));
                        }
                    });
                </script>
            </div>
        </div>
        <div style="overflow-x:auto;">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>@sortablelink('name', 'Name')</th>
                        <th>@sortablelink('vendor.vendor_name', 'Vendor Name')</th>
                        <th>@sortablelink('job_title', 'Job Title')</th>
                        <th>@sortablelink('city', 'City')</th>
                        <th>@sortablelink('email', 'Email')</th>
                        <th>@sortablelink('phone_number', 'Phone Number')</th>
                        <th colspan=2></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contacts as $contact)
                    <tr id="delete{{$contact->id}}">
                        <td>{{$contact->name}}</td>
                        <td><a href="/vendors/{{$contact->vendors_id}}">{{$contact->vendor->vendor_name}}</a></td>
                        <td>{{$contact->job_title}}</td>
                        <td>{{$contact->city}}</td>
                        <td>{{$contact->email}}</td>
                        <td>{{$contact->phone_number}}</td>
                        <td>
                            @if(auth()->user()->role->admin == 1)
                            <a href="{{ route('contacts.edit',$contact->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form class="delete" action="{{ route('contacts.destroy', $contact->id)}}" method="post">
                                @csrf @method('DELETE')
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {!! $contacts->appends(\Request::except('page'))->render() !!}
    </div>
</div>

<script>
    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>

@endsection
