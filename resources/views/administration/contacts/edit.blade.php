@extends('navbar')
@section('context')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update Contact</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('contacts.update', $contact->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value={{ $contact->name }} />
            </div>

            <div class="form-group">
                <label for="vendors_id">Company Name:</label>
                <select type="text" class="form-control selectpicker" name="vendors_id"
                        title="Select Vendor" value={{ $contact->vendors_id }}>
                  @foreach ($vendorDD_array as $data)
                    <option value="{{ $data->id }}"  >{{ $data->vendor_name }}</option>
                  @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email" value={{ $contact->email }} />
            </div>
            <div class="form-group">
                <label for="job_title">Position:</label>
                <input type="text" class="form-control" name="job_title" value={{ $contact->job_title }} />
            </div>
            <div class="form-group">
                <label for="city">City:</label>
                <input type="text" class="form-control" name="city" value={{ $contact->city }} />
            </div>
            <div class="form-group">
                <label for="phone_number">Phone Number:</label>
                <input type="tel" class="form-control" name="phone_number" pattern="[0-9]{3}-[0-9]{7}" title="sample 012-3456789" value={{ $contact->phone_number }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
