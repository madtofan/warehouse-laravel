@extends('navbar')
@section('context')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a product</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('products.update', $product->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="product_name">Product Name:</label>
                <input type="text" class="form-control" name="product_name" value={{ $product->product_name }} />
            </div>

            <div class="form-group">
                <label for="vendors_id">Vendor Name:</label>
                <select type="text" class="selectpicker form-control" name="vendors_id"
                        title="Select Vendor" value={{ $product->vendors_id }}>
                  @foreach ($vendorDD_array as $data)
                    <option value="{{ $data->id }}"  >{{ $data->vendor_name }}</option>
                  @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="price">Price:</label>
                <input type="number" step="0.01" class="form-control" name="price" value={{ $product->price }} />
            </div>

            <div class="form-group">
                <label for="low_threshold">Low Threshold:</label>
                <input type="number" class="form-control" name="low_threshold" value={{ $product->low_threshold }} />
            </div>

            <div class="form-group">
                <label for="category">Category:</label>
                <input type="text" class="form-control" name="category" value={{ $product->category }} />
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
              <button type="reset" value="reset">Clear Item</button>
        </form>
    </div>
</div>
@endsection
