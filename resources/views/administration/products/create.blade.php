@extends('navbar') @section('context')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                    <h3>Add Item To Product</h3>
                <div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br /> @endif
                        <form method="post" action="{{ route('products.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="product_name" class="col-md-3 col-form-label text-md-right">Product Name</label>
                                <div class="col-md-8">
                                    <input id="product_name" type="text" class="form-control" name="product_name" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="vendors_id" class="col-md-3 col-form-label text-md-right">Vendor Name</label>
                                <div class="col-md-8">
                                    <select type="text" class="form-control selectpicker" name="vendors_id" title="Select vendor">
                                        @foreach ($vendorDD_array as $data)
                                        <option value="{{ $data->id }}">{{ $data->vendor_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="price" class="col-md-3 col-form-label text-md-right">Price</label>
                                <div class="col-md-8">
                                    <input id="price" type="number" step="0.01" class="form-control" name="price" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="low_threshole" class="col-md-3 col-form-label text-md-right">Low Threshold</label>
                                <div class="col-md-8">
                                    <input id="low_threshold" type="number" class="form-control" name="low_threshold" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-md-3 col-form-label text-md-right">category</label>
                                <div class="col-md-8">
                                    <input id="category" type="text" class="form-control" name="category" required autofocus/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Add Item</button>
                                </div>
                            </div>
                        </form>
                    </div>
                  </div>
                </div>
              </div>
@endsection
