@extends('navbar') @section('context')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif

        </div>
        <div class="form-group row">
            <div class="col-sm-12">
                <h1 class="display-3"><img src="/images/icons/products.png" class="page-logo"/>Products Management</h1> @if(auth()->user()->role->admin == 1)
                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <a href="{{ route('products.create')}}" class="btn btn-primary">Register New Product</a>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <a class="btn download" href='/download/producttemplate.csv'> Download Template </a>
                    </div>
                </div>
                <form method='post' action='/uploadproductfile' enctype='multipart/form-data'>
                    {{ csrf_field() }}
                    <label for="file-upload" class="custom-file-upload">
                        <i class="fa fa-cloud-upload"></i> File Upload
                    </label>
                    <input id="file-upload" type="file" name='file' />

                    <input type='submit' name='submit' value='Import' class="btn btn-primary">
                </form>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-8">
                <label for="search" class="col-md-3 col-form-label text-md-right">Search product</label>
                <input id="search" autocomplete="off" type="text" class="typeahead form-control" name="search_value" value="{{ $filter_value }}">
                <script>
                    $('.typeahead').on('keydown', function(e) {
                        if (e.keyCode == 13) {
                            var search_text = document.getElementById("search").value;
                            window.location.replace("/products/filter/".concat(search_text));
                        }
                    });
                </script>
            </div>
        </div>

        <div style="overflow-x:auto;">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>@sortablelink('product_name', 'Product Name')</th>
                        <th>@sortablelink('vendor.vendor_name', 'Vendor Name')</th>
                        <th>@sortablelink('category', 'Category')</th>
                        <th>@sortablelink('price', 'Price')</th>
                        <th>@sortablelink('low_threshold', 'Low Threshold')</th>
                        <th colspan=3>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>{{$product->product_name}}</td>
                        <td><a href="/vendors/{{$product->vendors_id}}">{{$product->vendor->vendor_name}}</a></td>
                        <td>{{$product->category}}</td>
                        <td>RM {{number_format($product->price, 2)}}</td>
                        <td>{{$product->low_threshold}}</td>
                        @if(auth()->user()->role->admin == 1)
                        <td>
                            <a href="/list_product/{{$product->id}}" class="btn btn-primary">Check</a>
                        </td>
                        <td>
                            <a href="{{ route('products.edit',$product->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form class="delete" action="{{ route('products.destroy', $product->id)}}" method="post">
                                @csrf @method('DELETE')
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {!! $products->appends(\Request::except('page'))->render() !!}
    </div>
</div>

<script>
    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>
@endsection
