@extends('navbar') @section('context')
<div class="col-sm-12">

    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
</div>
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3"><img src="/images/icons/users.png" class="page-logo" />Users Management</h1> @if (Route::has('register'))
        <div>
            <a style="margin: 19px;" href="{{ route('register')}}" class="btn btn-primary">Register New User</a>
        </div>
        @endif
        <div class="form-group row">
            <div class="col-md-8">
                <label for="search" class="col-md-3 col-form-label text-md-right">Search users</label>
                <input id="search" autocomplete="off" type="text" class="typeahead form-control" name="search_value" value="{{ $filter_value }}">
                <script>
                    $('.typeahead').on('keydown', function(e) {
                        if (e.keyCode == 13) {
                            var search_text = document.getElementById("search").value;
                            window.location.replace("/users/filter/".concat(search_text));
                        }
                    });
                </script>
            </div>
        </div>
        <div style="overflow-x: auto;">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>@sortablelink('name', 'User Name')</th>
                        <th>@sortablelink('role.role_name', 'roleRoles')</th>
                        <th>@sortablelink('email', 'Email')</th>
                        <th colspan=2>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td><a href="/roles/{{$user->roles_id}}">{{$user->role->role_name}}</a></td>
                        <td>{{$user->email}}</td>
                        <td>
                            <a href="{{ route('users.edit',$user->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form class="delete" action="{{ route('users.destroy', $user->id)}}" method="post">
                                @csrf @method('DELETE')
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {!! $users->appends(\Request::except('page'))->render() !!}
    </div>
</div>

<script>
    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>
@endsection
