@extends('navbar') @section('context')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="display-3"><img src="/images/icons/logs.png" class="page-logo"/>Logs Management</h1>
                    <br/>
                    <br/>
                    <div class="row">

                        <form method="post" action="{{ route('logs.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-md-1 col-form-label text-md-right">FROM</label>

                                <input type="date" name="from" value="date">

                            </div>
                            <div class="form-group row">
                                <label class="col-md-1 col-form-label text-md-right">TO</label>

                                <input type="date" name="to" value="date">

                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <input type="submit" class="btn btn-primary" />
                                </div>
                            </div>
                        </form>
                    </div>

                    <div style="overflow-x:auto;">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>@sortablelink('description', 'Description')</th>
                                    <th>@sortablelink('item.products_id', 'Item Name')</th>
                                    <th>@sortablelink('item.quantity', 'Item Quantity')</th>
                                    <th>@sortablelink('executed_by.name', 'Executed By')</th>
                                    <th>@sortablelink('created_at', 'Timestamp')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($logs as $log)
                                <tr>
                                    <td>{{$log->description}}</td>
                                    <td>{{$log->item->product->product_name}}</td>
                                    <td>{{$log->item->quantity}}</td>
                                    <td>{{$log->executed_by->name}}</td>
                                    <td>{{$log->created_at}}</td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    {!! $logs->appends(\Request::except('page'))->render() !!}

                    <div class="form-group row">
                        <form method="post" action="report-download">
                            @csrf
                            <input value="{{ $to }}" name="to-table" type="hidden">
                            <input value="{{ $from }}" name="from-table" type="hidden">
                            <button type="submit" class="btn btn-primary"> Download PDF </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
