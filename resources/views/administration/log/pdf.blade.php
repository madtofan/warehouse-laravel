<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Warehouse App</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #000000;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>

<body>
    <div class="col-sm-12">

        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
    </div>
    <div class="row w3-container w3-green w3-cell">
        <div class="col-sm-12">
            <h1 class="display-3" style="text-align: center">Logs Report</h1>
            <hr>
            <div>
                <p>Report Generate At : {{ now()->toDateTimeString('Y-m-d H:i:s') }} </p>
                <p>Report Generate By : {{ auth()->user()->name }} </p>
            </div>
            <div class="row">

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>Description</td>
                            <td>Item Name</td>
                            <td>Item Quantity</td>
                            <td>Executed By</td>
                            <td>Timestamp</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($logs as $log)
                        <tr>
                            <td>{{$log->description}}</td>
                            <td>{{$log->item->product->product_name}}</td>
                            <td>{{$log->item->quantity}}</td>
                            <td>{{$log->executed_by->name}}</td>
                            <td>{{$log->created_at}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
</body>

</html>
