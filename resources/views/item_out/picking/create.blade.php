@extends('navbar') @section('context')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="display">
                <div class="display-header"></div>
                <div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br /> @endif
                    <div class="card-body">

                        <form method="post" action="{{ route('picking.store') }}">
                            @csrf
                            <div class="form-group">
                                <h3 class="display-6"> Create Picking Report </h3>
                            </div>
                            <br/>
                            <div class="form-group row">
                                <label for="createdBy" class="col-md-3 col-form-label text-md-right"> Created By </label>
                                <div class="col-md-8">
                                    <label>{{ auth()->user()->name }}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">Report Name</label>
                                <div class="col-md-8">
                                    <input id="report_name" type="text" name="name" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date" class="col-md-3 col-form-label text-md-right"> Report date </label>
                                <div class="col-md-8">
                                    <input id="date" type="date" name="date" />
                                </div>
                            </div>

                            <br />
                            <br />

                            <table style="width:70%">
                                <div class="form-group row">
                                    <thead>
                                        <tr>
                                            <th> Product Name </th>
                                            <th> Quantity </th>
                                        </tr>
                                    </thead>
                                </div>
                                <tbody id="picking">
                                    <tr id='row1'>
                                        <td>
                                            <div class="form-group autocomplete">
                                                <input class="search" autocomplete="off" type="text" name="product[1][id]" placeholder="Product">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group" name="quantity">
                                                <input type="number" name="product[1][quantity]" placeholder="10"/>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/>
                            <br/>
                            <p> <a href="javascript:;" id="add-product" class="btn btn-primary">Add Product</a> </p>

                            <p>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </p>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let i = 2;
    document.getElementById('add-product').onclick = function() {
        let template = `
        <tr>
        <td>
            <div class= "autocomplete">
               <input class="search" autocomplete="off" type="text" name="product[${i}][id]" placeholder="Product">
            </div>
        </td>
        <td>
            <div class="form-group" name="quantity">
                <input type="number" name="product[${i}][quantity]" placeholder="10"/>
            </div>
        </td>
    </tr>
    <td>
        <div class="form-group">
            <button class="buang btn-danger" onclick="buang(${i})" id="delete" name="delete[${i}][item]"> X </button>
        </div>
    </td>
    `;

        let container = document.getElementById('picking');
        let div = document.createElement('tr');
        div.setAttribute('id', `row${i}`);
        div.innerHTML = template;
        container.appendChild(div);

        i++;

        $('.search').trigger('added');
    }

    function buang(rowNumber) {
        document.getElementById(`row` + rowNumber).remove();
    }
</script>
<script type="text/javascript">
        var path = "{{ url('searchProduct') }}";
        var typeaheadSettings = {
            minLength: 0,
            source: function(query, process) {
                return $.get(path, {
                    query: query
                }, function(data) {
                    return process(data);
                });
            }
        }
        $('.search').typeahead(typeaheadSettings);

        $('.search').on('added', function(){
            $('.search').typeahead(typeaheadSettings);
        });
    </script>
@endsection
