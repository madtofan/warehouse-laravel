@extends('navbar') @section('context') @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<br /> @endif
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="display">
                <div class="display-header">Item to pick up : {{$item->product->product_name}}</div>
                <br/>
                <div class="display-body">
                    <form method="post" action="/pick_item">
                        @method('POST') @csrf
                        <div class="form-group row">
                            <label for="shelf_number" class="col-md-4 col-form-label text-md-right">Shelf</label>
                          </div>
                          <div class="form-group row">
                              <label for="number-of-item" class="col-md-3 col-form-label text-md-right">Item Quantity</label>
                              <div class="col-md 8">
                                <input id="number-of-item" type="number" min="1" max="{{ $item_left }}" class="form-control{{ $errors->has('number-of-item') ? ' is-invalid' : '' }}" name="number-of-item" value="{{ $item_left }}" required autofocus> @if ($errors->has('number-of-item'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('number-of-item') }}</strong>
                                </span> @endif
                            </div>
                          </div>

                          <div class="form-group row">
                              <label for="select_shelf" class="col-md-3 col-form-label text-md-right">Shelf Number</label>
                              <div class="col-md 8">
                                <input id="select_shelf" type="text" class="form-control{{ $errors->has('select_shelf') ? ' is-invalid' : '' }}" name="select_shelf" value="{{ old('select_shelf') }}" required autofocus> @if ($errors->has('select_shelf'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('select_shelf') }}</strong>
                                </span> @endif
                            </div>
                          </div>

                        </div>
                        <input id="item" type="hidden" name="item" value="{{ $item->id }}">
                        <input id="item-left" type="hidden" name="item-left" value="{{ $item_left }}"> @foreach($add_shelf as $list_shelf)
                        <input id="add-list" type="hidden" name="shelf-list[]" value="{{ $list_shelf }}"> @endforeach @foreach($add_quantity as $list_quantity)
                        <input id="add-list" type="hidden" name="quantity-list[]" value="{{ $list_quantity }}"> @endforeach
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Pick from Inventory
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
