@extends('navbar')

@section('context')
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Packing: {{$report_items[0]->report->report_name}}</h1>
    <div>
      <a style="margin: 19px;" href="{{ route('packing.index')}}" class="btn btn-primary">Back to Report List</a>
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <th>@sortablelink('product.product_name', 'Name')</th>
          <th>@sortablelink('quantity', 'Quantity')</th>
          <th>@sortablelink('status.status_name', 'Status')</th>
          <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($report_items as $item)
        <tr>
            <td><a href="/products/{{$item->product->id}}">{{$item->product->product_name}}</a></td>
            <td>{{$item->quantity}}</td>
            <td>{{$item->status->status_name}}</td>
            <td>
              @if ($item->statuses_id == 6)
                <a href="{{ route('packing.edit', $item->id)}}" class="btn btn-primary">Pack item</a>
              @endif
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
</div>
</div>
@endsection
