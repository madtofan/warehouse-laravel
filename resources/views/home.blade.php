@extends('navbar')
@section('context')
@if (session('status'))
@endif
<div class="outer-card">
    <div class="card" style="text-align:center;">
        <div class="card-header">Dashboard</div>
        <div class="card-body">
            <div class="panel-body"><strong>Success!</strong>
                <div>You are logged in as {{ Auth::user()->name }}!</div>
            </div>
            <div class="panel-body">Your authorization level is
                @if(auth()->user()->role->admin == 1) Admin
                @else Normal User
                @endif
            </div>
        </div>
    </div>
</div>
@foreach($low_products as $low_product)
<div class="outer-card">
    <div class="card">
        <div class="card-header alert">WARNING! Low Product</div>
        <div class="card-body"><strong>{{$low_product->product_name}}</strong> is now low, expect to have <strong>{{$low_product->low_threshold}}</strong> but currently have <strong>{{$low_product->quantity}}</strong></div>
    </div>
</div>
@endforeach
@endsection
