@extends('base') @section ('main') @guest @else
<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>W</b>A</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Warehousing <b>APP</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">{{ Auth::user()->name }}</li>
            <!-- Receiving / Put Away Dropdown -->
            <li class="treeview">
                <a href="#"><i class="fa fa-truck fa-flip-horizontal"></i> <span>Receiving / Put Away</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
                <ul class="treeview-menu">
                    <li><a href="/receiving">Receiving</a></li>
                    <li><a href="/putaway">Put Away</a></li>
                </ul>
            </li>
            <!-- Picking / Packing Dropdown -->
            <li class="treeview">
                <a href="#"><i class="fa fa-truck"></i> <span>Picking / Packing</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
                <ul class="treeview-menu">
                    <li><a href="/picking">Picking</a></li>
                    <li><a href="/packing">Packing</a></li>
                </ul>
            </li>
            <!-- Inventory Management Dropdown -->
            <li class="treeview">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Inventory Management</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
                <ul class="treeview-menu">
                    <li><a href="/summary">Inventory List</a></li>
                    <li><a href="/list_shelf/1">Shelf Inventory</a></li>
                    <li><a href="/list_product/1">Product Inventory</a></li>
                </ul>
            </li>
            <!-- Administration Dropdown -->
            <li class="treeview">
                <a href="#"><i class="fa fa-user-circle"></i> <span>Administration</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
                <ul class="treeview-menu">
                    <li><a href="/users">Users Management</a></li>
                    <li><a href="/roles">Roles Management</a></li>
                    <li><a href="/logs">Logs Management</a></li>
                    <li><a href="/products">Products Management</a></li>
                    <li><a href="/contacts">Contacts Management</a></li>
                    <li><a href="/vendors">Vendors Management</a></li>
                </ul>
            </li>

            <li class="pull-down">
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
 document.getElementById('logout-form').submit();">
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">Logout</span>
                </a>
            </li>
            <!-- /.sidebar-menu -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
@endif

<div class="content-wrapper">
    @yield('context')
    <!-- /.content-wrapper -->
</div>
<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <div>Icons made by <a href="https://www.flaticon.com/authors/pause08" title="Pause08">Pause08</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="http://idsis.my">IDS Integrated Solutions Sdn. Bhd.</a></strong>
</footer>
@endsection
