@extends('navbar') @section('context')
    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <h2 class="display-3"><img src="/images/icons/shelves-recheck.png" class="page-logo" />Shelf Inventory</h2>

            <form id="search_shelf" method="post" action="{{ route('list_shelf.store') }}">
                @csrf
                <div class="form-group">
                    <label for="Shelf">Shelf</label>
                    <input id="search" autocomplete="off" type="text" class="form-control" name="search_value" value="{{$current_shelf}}">
                </div>
            </form>
            <script>
                $('.typeahead').on('keydown', function(e) {
                    if (e.keyCode == 13) {
                        var ta = $(this).data('typeahead');
                        var val = ta.$menu.find('.active').data('value');
                        if (!val)
                            $('#search_shelf').submit();
                    }
                });
            </script>
        </div>
      </div>

        <div style="overflow-x:auto;">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($shelfs as $row)
                    <tr>
                        <td><a href="/list_product/{{$row->id}}">{{ $row->product_name }}</a></td>
                        <td>{{ $row->Quantity }} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $shelfs->links() }}

        <script type="text/javascript">
            var path = "{{ url('searchShelf') }}";
            $('#search').typeahead({
                minLength: 0,
                source: function(query, process) {
                    return $.get(path, {
                        query: query
                    }, function(data) {
                        return process(data);
                    });
                }
            });
        </script>
@endsection
