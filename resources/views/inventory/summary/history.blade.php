@extends('navbar') @section('context') @if(session()->get('success'))
<div class="alert alert-success">
    {{ session()->get('success') }}
</div>
@endif
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3"><img src="/images/icons/history.png" class="page-logo" />Inventory History</h1>
        <div style="overflow-x:auto;">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Product Name</td>
                    <td>Quantity</td>
                    <td>Shelf</td>
                    <td>Date Added</td>
                </tr>
            </thead>
            <tbody>
                @foreach($inventory_history as $inventory)
                <tr>
                    <td><a href="/products/{{$inventory->product->id}}">{{$inventory->product->product_name}}</a></td>
                    <td>{{$inventory->quantity}}</td>
                    <td><a href="/list_shelf/{{$inventory->shelf}}">{{$inventory->shelf}}</a></td>
                    <td>{{$inventory->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
        {{ $inventory_history->links() }}
    </div>
</div>
@endsection
