@extends('navbar') @section('context')

    @if(session()->get('fail-search'))
    <div class="alert alert-danger">
        {{ session()->get('fail-search') }}
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3"><img src="/images/icons/products-recheck.png" class="page-logo" />Product Inventory</h1>
            <form id="search_product" method="post" action="{{ route('list_product.store') }}">
                @csrf
                <div class="form-group">
                    <label for="Product">Product</label>
                    <input id="search" autocomplete="off" type="text" class="form-control" name="search_value" value="{{$current_list->product_name}}">
                </div>
                <input type="hidden" value="{{$current_list->id}}" name="current-id">
            </form>
            <script>
                $('.typeahead').on('keydown', function(e) {
                    if (e.keyCode == 13) {
                        var ta = $(this).data('typeahead');
                        var val = ta.$menu.find('.active').data('value');
                        if (!val)
                            $('#search_product').submit();
                    }
                });
            </script>
            <div id="product_list"></div>
        </div>
    </div>

    <div style="overflow-x:auto;">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Shelf Number</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                @foreach($product_list as $row)
                <tr>
                    <td><a href="/list_shelf/{{$row->shelf}}">{{$row->shelf}}</a></td>
                    <td>{{$row->Quantity}}
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $product_list->links() }}
    <script type="text/javascript">
        var path = "{{ url('searchProduct') }}";
        $('#search').typeahead({
            minLength: 0,
            source: function(query, process) {
                return $.get(path, {
                    query: query
                }, function(data) {
                    return process(data);
                });
            }
        });
    </script>
@endsection
