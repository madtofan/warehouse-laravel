@extends('base') @section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Add Summary Information</h1>
        <div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br /> @endif
            <form method="post" action="{{ route('summary.store') }}">
                @csrf
                <div class="form-group">
                    <label for="products_id">Product Name:</label>
                    <select type="text" class="selectpicker" name="products_id" title="Select vendor">
                        @foreach ($productsDD_array as $data)
                        <option value="{{ $data->id }}">{{ $data->product_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="quantity">Quantity</label>
                    <input type="text" class="form-control" name="quantity" />
                </div>

                <div class="form-group">
                    <label for="shelf">Shelf</label>
                    <input type="number" class="form-control" name="shelf" />
                </div>
                <button type="submit" class="btn btn-primary-outline">Add Inventory</button>
            </form>
        </div>
    </div>
</div>
@endsection
