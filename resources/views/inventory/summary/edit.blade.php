@extends('base')
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update Summary</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('summary.update', $inventory->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="products_id">Product Ref ID</label>
                <input type="number" class="form-control" name="products_id" value={{ $inventory->products_id }} />
            </div>
            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number" class="form-control" name="quantity" value={{ $inventory->quantity }} />
            </div>
            <div class="form-group">
                <label for="shelf">Shelf</label>
                <input type="number" class="form-control" name="shelf" value={{ $inventory->shelf }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
