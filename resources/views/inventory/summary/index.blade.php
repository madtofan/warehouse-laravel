@extends('navbar') @section('context') @if(session()->get('success'))
<div class="alert alert-success">
    {{ session()->get('success') }}
</div>
@endif
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3"><img src="/images/icons/summary.png" class="page-logo" />Inventory List</h1>
        <div class="form-group row">
            <div class="col-md-8">
                <label for="search" class="col-md-3 col-form-label text-md-right">Search Product</label>
                <input id="search" autocomplete="off" type="text" class="typeahead form-control" name="search_value" value="{{ $filter_value }}">
                <script>
                    $('.typeahead').on('keydown', function(e) {
                        if (e.keyCode == 13) {
                            var search_text = document.getElementById("search").value;
                            window.location.replace("/summary/filter/".concat(search_text));
                        }
                    });
                </script>
            </div>
        </div>
        <div style="overflow-x:auto;">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($inventories as $inventory)
                    <tr>
                        <td><a href="/products/{{$inventory->id}}">{{$inventory->product_name}}</a></td>
                        <td>{{$inventory->Quantity}}</td>
                        <td>
                            <a href="/list_product/{{$inventory->id}}" class="btn btn-primary">Check</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $inventories->links() }}
    </div>
</div>
@endsection
