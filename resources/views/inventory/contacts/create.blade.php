@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add Contact Information</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('contacts.store') }}">
          @csrf
          <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" name="name"/>
          </div>

          <div class="form-group">
              <label for="vendors_id">Company Name:</label>
              <select type="text" class="selectpicker" name="vendors_id"
                      title="Select vendor">
                @foreach ($vendorDD_array as $data)
                  <option value="{{ $data->id }}"  >{{ $data->vendor_name }}</option>
                @endforeach
              </select>
          </div>

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" name="email"/>
          </div>
          <div class="form-group">
              <label for="job_title">Position:</label>
              <input type="text" class="form-control" name="job_title"/>
          </div>
          <div class="form-group">
              <label for="city">City:</label>
              <input type="text" class="form-control" name="city"/>
          </div>
          <div class="form-group">
              <label for="phone_number">Phone Number:</label>
              <input type="tel" class="form-control" name="phone_number" pattern="[0-9]{3}-[0-9]{7}" title="sample 012-3456789"/>
          </div>
          <button type="submit" class="btn btn-primary-outline">Add Contact</button>
      </form>
  </div>
</div>
</div>
@endsection
