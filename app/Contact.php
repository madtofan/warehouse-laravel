<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Contact extends Model
{
    use Sortable;
    
    protected $fillable = [
        'name',
        'vendors_id',
        'email',
        'city',
        'phone_number',
        'job_title'
    ];

    public $sortable = ['name', 'vendors_id', 'email', 'city', 'phone_number', 'job_title'];

    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendors_id', 'id');
    }
}
