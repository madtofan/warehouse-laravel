<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Vendor extends Model
{
    use Sortable;

    protected $fillable = [
      'vendor_name',
      'vendor_address',
      'vendor_email',
      'vendor_number'
  ];

    public $sortable = ['vendor_name', 'vendor_address', 'vendor_email', 'vendor_number'];

    public function contacts()
    {
        return $this->hasMany('App\Contact', 'contacts_id');
    }

    public function products()
    {
        return $this->hasMany('App\Product', 'products_id');
    }
    public static function insertData($data)
    {
        $ignore_count = 0;
        $value=DB::table('vendors')->where('vendor_name', $data['vendor_name'])->get();
        if ($value->count() == 0) {
            DB::table('vendors')->insert($data);
        } else {
            $ignore_count = 1;
        }
        return $ignore_count;
    }
}
