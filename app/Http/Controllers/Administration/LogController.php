<?php

namespace App\Http\Controllers\Administration;

use App\Log;
use PDF;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function redirectTo($request)
    {
        return route('login');
    }

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Request $request)
    {
        $logs = Log::sortable()->paginate(10);
        $to = '';
        $from ='';

        return view('administration.log.index', compact('logs', 'to', 'from'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from = $request->get('from');
        $to = $request->get('to');

        $logs = Log::whereBetween('created_at', [$from, $to])->
        sortable()->
        paginate(10);

        return view('administration.log.index', compact('logs', 'to', 'from'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function report(Request $request)
    {
        $from = $request->get('from-table');
        $to = $request->get('to-table');

        if ($from=='') {
            $data = ['logs'=> Log::all()];
        } else {
            $data = ['logs'=>Log::whereBetween('created_at', [$from, $to])->get()];
        }

        $pdf = PDF::loadView('administration.log.pdf', $data);
        return $pdf->download('logs.pdf');
    }
}
