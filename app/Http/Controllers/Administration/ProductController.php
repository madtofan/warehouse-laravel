<?php

namespace App\Http\Controllers\Administration;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;

use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
    * Get the path the user should be redirected to.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return string
    */
    protected function redirectTo($request)
    {
        return route('login');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::sortable()->paginate(10);

        $filter_value = "";

        return view('administration.products.index', compact('products', 'filter_value'));
    }

    public function filter($filter)
    {
        $products = Product::where('product_name', 'LIKE', '%'.$filter.'%')->
      sortable()->
      paginate(10)->
      appends('product_name', 'LIKE', '%'.$filter.'%');

        $filter_value = $filter;

        return view('administration.products.index', compact('products', 'filter_value'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->role->admin) {
            return redirect('/')->with('status', 'Admin Access Required');
        } else {
            return view('administration.products.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'product_name'=>'required',
          'vendors_id'=>'required',
          'category'=>'required',
          'price'=>'required',
          'low_threshold' => 'required',
        ]);

        $product = new Product([
          'product_name' => $request->get('product_name'),
          'vendors_id' => $request->get('vendors_id'),
          'category' => $request->get('category'),
          'price' => $request->get('price'),
          'low_threshold' => $request->get('low_threshold')
        ]);
        $product->save();
        return redirect('/products')->with('success', 'Product saved!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::where('id', '=', $id)->
      sortable()->
      paginate(10);

        $filter_value = $products->first()->product_name;

        return view('administration.products.index', compact('products', 'filter_value'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('administration.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'product_name'=>'required',
        'vendors_id'=>'required',
        'category'=>'required',
        'price'=>'required',
      ]);

        $product = Product::find($id);
        $product->product_name = $request->get('product_name');
        $product->vendors_id = $request->get('vendors_id');
        $product->category = $request->get('category');
        $product->price = $request->get('price');
        $product->low_threshold = $request->get('low_threshold');
        $product->save();

        return redirect('/products')->with('success', 'Product updated!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect('/products')->with('success', 'Product deleted!!');
    }
    public function uploadFile(Request $request)
    {
        $request->validate([
        'file'=>'required',
      ]);
        if ($request->input('submit') != null) {
            $file = $request->file('file');
            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv");

            // 2MB in Bytes
            $maxFileSize = 2097152;

            // Check file extension
            if (in_array(strtolower($extension), $valid_extension)) {

        // Check file size
                if ($fileSize <= $maxFileSize) {

          // File upload location
                    $location = 'uploads';

                    // Upload file
                    $file->move($location, $filename);

                    // Import CSV to Database
                    $filepath = public_path($location."/".$filename);

                    // Reading file
                    $file = fopen($filepath, 'r');

                    $importData_arr = array();
                    $i = 0;

                    while (($filedata = fgetcsv($file, 1000, ",")) !== false) {
                        $num = count($filedata);

                        // Skip first row (Remove below comment if you want to skip the first row)
                        if ($i == 0) {
                            $i++;
                            continue;
                        }
                        for ($c=0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata [$c];
                        }
                        $i++;
                    }
                    fclose($file);
                    unlink($filepath);
                    $ignore_count = 0;
                    // Insert to MySQL database
                    foreach ($importData_arr as $importData) {
                        $insertData = array(
               "product_name"=>$importData[0],
               "vendors_id"=>$importData[1],
               "category"=>$importData[2],
               "price"=>$importData[3],
               "low_threshold"=>$importData[4]);
                        if (Product::insertData($insertData) > 0) {
                            $ignore_count += 1;
                        }
                    }

                    if ($ignore_count > 0) {
                        $message = 'Import Successful.'.PHP_EOL.'Ignored Lines '. $ignore_count;
                    } else {
                        $message = 'Import Successful.';
                    }
                } else {
                    $message = 'File too large. File must be less than 2MB.';
                }
            } else {
                $message = 'Invalid File Extension.';
            }
        }

        // Redirect to index
        return redirect()->action('Administration\ProductController@index')->with('success', $message);
    }
}
