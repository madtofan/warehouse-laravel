<?php

namespace App\Http\Controllers\Administration;
use App\Role;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class RoleController extends Controller
{
  /**
  * Get the path the user should be redirected to.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return string
  */
  protected function redirectTo($request)
  {
    return route('login');
  }

  public function __construct()
  {
    $this->middleware('admin');
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::sortable()->paginate(10);

        $filter_value = "";

        return view('administration.roles.index', compact('roles', 'filter_value'));
    }

   public function filter($filter)
   {
       $roles = Role::where('role_name', 'LIKE', '%'.$filter.'%')->
       sortable()->
       paginate(10)->
       appends('role_name', 'LIKE', '%'.$filter.'%');

       $filter_value = $filter;

       return view('administration.roles.index', compact('roles', 'filter_value'));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administration.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'role_name'=>'required',
          'admin'=>'required',
          'moderator'=>'required',
          'receiving'=>'required',
          'inventory'=>'required',
          'putting_away'=>'required'
        ]);
        $role = new Role([
          'role_name' => $request->get('role_name'),
          'admin' => $request->get('admin'),
          'moderator' => $request->get('moderator'),
          'receiving' => $request->get('receiving'),
          'inventory' => $request->get('inventory'),
          'putting_away' => $request->get('putting_away')
        ]);
        $role->save();
        return redirect('/roles')->with('success', "Roles saved!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Role::sortable()->
        where('id', '=', $id)->
        paginate(10);

        $filter_value = $roles->first()->role_name;

        return view('administration.roles.index', compact('roles', 'filter_value'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        return view('administration.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'role_name'=>'required',
          'admin'=>'required',
          'moderator'=>'required',
          'receiving'=>'required',
          'inventory'=>'required',
          'putting_away'=>'required'
        ]);

        $role = Role::find($id);
        $role->role_name = $request->get('role_name');
        $role->admin = $request->get('admin');
        $role->moderator = $request->get('moderator');
        $role->receiving = $request->get('receiving');
        $role->inventory = $request->get('inventory');
        $role->putting_away = $request->get('putting_away');
        $role->save();

        return redirect('/roles')->with('success', 'Roles updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();

        return redirect('/roles')->with('success', 'Roles deleted!');
    }
}
