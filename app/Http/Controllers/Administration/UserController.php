<?php

namespace App\Http\Controllers\Administration;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
  /**
  * Get the path the user should be redirected to.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return string
  */
  protected function redirectTo($request)
  {
    return route('login');
  }

  public function __construct()
  {
    $this->middleware('admin');
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::sortable()->paginate(10);

        $filter_value = "";

        return view('administration.users.index', compact('users', 'filter_value'));
    }

    public function filter($filter)
    {
        $users = User::where('name', 'LIKE', '%'.$filter.'%')->
        sortable()->
        paginate(10)->
        appends('name', 'LIKE', '%'.$filter.'%');

        $filter_value = $filter;

        return view('administration.users.index', compact('users', 'filter_value'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administration.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::where('id', '=', $id)->
        sortable()->
        paginate(10);

        $filter_value = $users->first()->name;

        return view('administration.users.index', compact('users', 'filter_value'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('administration.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'name'=>'required',
          'password'=>'required|string|min:8'
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->password = Hash::make($request->get('password'));
        $user->email = $request->get('email');
        $user->save();

        return redirect('/users')->with('success', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/users')->with('success', 'User deleted!');
    }
}
