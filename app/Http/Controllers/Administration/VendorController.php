<?php

namespace App\Http\Controllers\Administration;

use App\Vendor;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class VendorController extends Controller
{
    /**
    * Get the path the user should be redirected to.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return string
    */
    protected function redirectTo($request)
    {
        return route('login');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::sortable()->paginate(10);
        $filter_value = "";

        return view('administration.vendors.index', compact('vendors', 'filter_value'));
    }

    public function filter($filter)
    {
        $vendors = Vendor::where('vendor_name', 'LIKE', '%'.$filter.'%')->
       sortable()->
       paginate(10)->
       appends('vendor_name', 'LIKE', '%'.$filter.'%');

        $filter_value = $filter;

        return view('administration.vendors.index', compact('vendors', 'filter_value'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->role->admin) {
            return redirect('/')->with('status', 'Admin Access Required');
        } else {
            return view('administration.vendors.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'vendor_name'=>'required',
          'vendor_number'=>'required'
        ]);

        $vendor = new Vendor([
            'vendor_name' => $request->get('vendor_name'),
            'vendor_address' => $request->get('vendor_address'),
            'vendor_email' => $request->get('vendor_email'),
            'vendor_number' => $request->get('vendor_number')
          ]);
        $vendor->save();
        return redirect('/vendors')->with('success', 'List saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vendors = Vendor::where('id', '=', $id)->sortable()->paginate(10);
        $filter_value = $vendors->first()->vendor_name;

        return view('administration.vendors.index', compact('vendors', 'filter_value'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::find($id);
        return view('administration.vendors.edit', compact('vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'vendor_name'=>'required',
          'vendor_number'=>'required'
        ]);

        $vendor = Vendor::find($id);
        $vendor->vendor_name = $request->get('vendor_name');
        $vendor->vendor_address = $request->get('vendor_address');
        $vendor->vendor_email = $request->get('vendor_email');
        $vendor->vendor_number = $request->get('vendor_number');
        $vendor->save();

        return redirect('/vendors')->with('success', 'List updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::find($id);
        $vendor->delete();

        return redirect('/vendors')->with('success', 'List deleted!');
    }

    public function uploadFile(Request $request)
    {
        $request->validate(['file'=>'required',]);
        if ($request->input('submit') != null) {
            $file = $request->file('file');

            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            $valid_extension = array("csv");
            $maxFileSize = 2097152;

            if (in_array(strtolower($extension), $valid_extension)) {
                if ($fileSize <= $maxFileSize) {
                    $location = 'uploads';
                    $file->move($location, $filename);
                    $filepath = public_path($location."/".$filename);
                    $file =fopen($filepath, "r");
                    $importData_arr = array();
                    $i = 0;

                    while (($filedata = fgetcsv($file, 1000, ",")) !== false) {
                        $num = count($filedata);

                        if ($i == 0) {
                            $i++;
                            continue;
                        }
                        for ($c=0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata [$c];
                        }
                        $i++;
                    }
                    fclose($file);
                    unlink($filepath);

                    $ignore_count = 0;
                    foreach ($importData_arr as $importData) {
                        $insertData = array(
                "vendor_name"=>$importData[0],
                "vendor_address"=>$importData[1],
                "vendor_email"=>$importData[2],
                "vendor_number"=>$importData[3]);

                        if (Vendor::insertData($insertData) > 0) {
                            $ignore_count += 1;
                        }
                    }
                    if ($ignore_count > 0) {
                        $message = 'Import Successful.'.' Ignored '.$ignore_count.'Lines';
                    } else {
                        $message = 'Import Successful.';
                    }
                } else {
                    $message = 'File too large. File must be less than 2MB.';
                }
            } else {
                $message = 'Invalid File Extension.';
            }
        }
        return redirect()->action('Administration\VendorController@index')->with('success', $message);
    }
}
