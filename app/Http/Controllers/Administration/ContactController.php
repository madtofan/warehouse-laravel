<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;

use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
    * Get the path the user should be redirected to.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return string
    */
    protected function redirectTo($request)
    {
        return route('login');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::sortable()->paginate(10);
        $filter_value = "";

        return view('administration.contacts.index', compact('contacts', 'filter_value'));
    }

    public function filter($filter)
    {
        $contacts = Contact::where('name', 'LIKE', '%'.$filter.'%')->
     sortable()->
     paginate(10)->
     appends('name', 'LIKE', '%'.$filter.'%');

        $filter_value = $filter;

        return view('administration.contacts.index', compact('contacts', 'filter_value'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->role->admin) {
            return redirect('/')->with('status', 'Admin Access Required');
        } else {
            return view('administration.contacts.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'vendors_id'=>'required',
            'phone_number'=>'required',
            'email'=>'required'
        ]);

        $contact = new Contact([
            'name' => $request->get('name'),
            'vendors_id' => $request->get('vendors_id'),
            'email' => $request->get('email'),
            'job_title' => $request->get('job_title'),
            'city' => $request->get('city'),
            'phone_number' => $request->get('phone_number')
        ]);
        $contact->save();
        return redirect('/contacts')->with('success', 'Contact saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contacts = Contact::where('vendors_id', '=', $id)->sortable()->paginate(10);
        $filter_value = $contacts->first()->name;

        return view('administration.contacts.index', compact('contacts', 'filter_value'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::find($id);
        return view('administration.contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'vendors_id'=>'required',
            'phone_number'=>'required',
            'email'=>'required'
        ]);

        $contact = Contact::find($id);
        $contact->name =  $request->get('name');
        $contact->vendors_id = $request->get('vendors_id');
        $contact->email = $request->get('email');
        $contact->job_title = $request->get('job_title');
        $contact->city = $request->get('city');
        $contact->phone_number = $request->get('phone_number');
        $contact->save();

        return redirect('/contacts')->with('success', 'Contact updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();

        return redirect('/contacts')->with('success', 'contact deleted!!!!');
    }
}
