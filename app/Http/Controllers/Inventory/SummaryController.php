<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Inventory;
use App\Product;

use App\Http\Controllers\Controller;

class SummaryController extends Controller
{
  /**
  * Get the path the user should be redirected to.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return string
  */
  protected function redirectTo($request)
  {
    return route('login');
  }

  public function __construct()
  {
    $this->middleware('auth');
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inventories = Inventory::leftJoin('products', 'inventories.products_id', '=', 'products.id')->
                select('products.id','products.product_name', DB::raw('SUM(quantity) as Quantity'))->
                groupBy('products.id')->
                paginate(10);

        $filter_value = "";

        return view('inventory.summary.index', compact('inventories', 'filter_value'));
    }

    public function filter($filter)
    {
      $inventories = Inventory::leftJoin('products', 'inventories.products_id', '=', 'products.id')->
              select('products.id','products.product_name', DB::raw('SUM(quantity) as Quantity'))->
              where('products.product_name', 'LIKE', '%'.$filter.'%')->
              groupBy('products.id')->
              paginate(10)->
              appends('products.product_name', 'LIKE', '%'.$filter.'%');

      $filter_value = $filter;

        return view('inventory.summary.index', compact('inventories', 'filter_value'));
    }

    public function history()
    {
        $inventory_history = Inventory::orderBy('created_at', 'desc')->paginate(10);
            return view('inventory.summary.history', compact('inventory_history'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('inventory.summary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
            'products_id'=>'required',
            'quantity'=>'required',
            'shelf'=>'required'
        ]);

        $inventory = new Inventory([
            'products_id' => $request->get('products_id'),
            'quantity' => $request->get('quantity'),
            'shelf' => $request->get('shelf')
        ]);
        $inventory->save();
        return redirect('/summary')->with('success', 'Inventory saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inventory = Inventory::find($id);
        return view('inventory.summary.edit', compact('inventory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $request->validate([
              'products_id'=>'required',
              'quantity'=>'required',
              'shelf'=>'required'
          ]);

          $inventory = Inventory::find($id);
          $inventory->products_id =  $request->get('products_id');
          $inventory->quantity = $request->get('quantity');
          $inventory->shelf = $request->get('shelf');
          $inventory->save();

          return redirect('/summary')->with('success', 'Inventory updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $inventory = Inventory::find($id);
      $inventory->delete();

      return redirect('/summary')->with('success', 'Inventory deleted!');
    }
}
