<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Inventory;
use App\Product;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;

use App\Http\Controllers\Controller;

class RecheckShelfController extends Controller
{
  /**
  * Get the path the user should be redirected to.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return string
  */
  protected function redirectTo($request)
  {
    return route('login');
  }

  public function __construct()
  {
    $this->middleware('auth');
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current_shelf = $request->get('search_value');

        $shelfs = Inventory::leftJoin('products', 'inventories.products_id', '=', 'products.id')->
        select('products.product_name', DB::raw('SUM(inventories.quantity) as Quantity'))->
        where('inventories.shelf', '=', $current_shelf)->
        groupBy('products.product_name')->
        paginate(10);

      return view('inventory.summary.indexRecheckShelf', compact('shelfs','current_shelf'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $current_shelf = $id;

      $shelfs = Inventory::leftJoin('products', 'inventories.products_id', '=', 'products.id')->
      select('products.id','products.product_name', DB::raw('SUM(inventories.quantity) as Quantity'))->
      where('inventories.shelf', '=', $current_shelf)->
      groupBy('products.product_name')->
      paginate(10);

      return view('inventory.summary.indexRecheckShelf', compact('shelfs','current_shelf'));
    }

    public function searchShelf(Request $request)
    {
        $result= Inventory::select('shelf')->
        where('shelf', 'LIKE', "%{$request->input('query')}%")->
        distinct()->
        limit(5)->
        get();

        $jsonArray = array();

        foreach($result as $data){
            $jsonArray[] = trim(json_encode($data->shelf), '"');
        }

        return $jsonArray;
    }

    public function select(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
    }
}
