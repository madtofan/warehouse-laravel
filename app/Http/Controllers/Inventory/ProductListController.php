<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Inventory;
use App\Product;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;

use App\Http\Controllers\Controller;

class ProductListController extends Controller
{
  /**
  * Get the path the user should be redirected to.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return string
  */
  protected function redirectTo($request)
  {
    return route('login');
  }

  public function __construct()
  {
    $this->middleware('auth');
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current_list = Product::where('product_name', $request->get('search_value'))->get();

        if ($current_list->count() > 0) {
          $current_list = $current_list->first();
          $product_list = Inventory::leftJoin('products', 'inventories.products_id', '=', 'products.id')->
          select('inventories.shelf', DB::raw('SUM(quantity) as Quantity'))->
          where('products.id', '=', $current_list->id)->
          groupBy('inventories.shelf')->
          paginate(10);

          return view('inventory.summary.indexProductList', compact('product_list','current_list'));
        }
        else{
          $current_list = Product::where('id', $request->get('current-id'))->get()[0];

          $product_list = Inventory::leftJoin('products', 'inventories.products_id', '=', 'products.id')->
          select('inventories.shelf', DB::raw('SUM(quantity) as Quantity'))->
          where('products.id', '=', $request->get('current-id'))->
          groupBy('inventories.shelf')->
          paginate(10);

          return view('inventory.summary.indexProductList', compact('product_list','current_list'))->with('fail-search', 'No record found for '.$request->get('search_value'));
        }
    }

    public function searchProduct(Request $request)
    {
        $result= Product::select('product_name')->
        where('product_name', 'LIKE', "%{$request->input('query')}%")->
        distinct()->
        limit(5)->
        get();

        $jsonArray = array();

        foreach($result as $data){
            $jsonArray[] = trim(json_encode($data->product_name), '"');
        }

        return $jsonArray;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $current_list = Product::where('id', $id)->get()[0];

      $product_list = Inventory::leftJoin('products', 'inventories.products_id', '=', 'products.id')->
      select('inventories.shelf', DB::raw('SUM(quantity) as Quantity'))->
      where('products.id', '=', $id)->
      groupBy('inventories.shelf')->
      paginate(10);

      return view('inventory.summary.indexProductList', compact('product_list','current_list'));
    }

    public function select(Request $request)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
    }
}
