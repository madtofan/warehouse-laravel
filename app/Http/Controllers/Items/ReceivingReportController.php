<?php

namespace App\Http\Controllers\Items;

use App\Product;
use App\Report;
use App\Items;
use App\User;
use App\Log;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class ReceivingReportController extends Controller
{
    protected function redirectTo($request)
    {
        return route('login');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $status_id = 2;
        $reports = Report::rightJoin('items', 'reports.id', '=', 'items.report_id')->
    select('reports.id', 'reports.report_name', 'reports.generated_by', 'reports.execution_date')->
    distinct()->
    where('items.statuses_id', '=', $status_id)->
    sortable()->
    paginate(10);

        return view('item_in.receiving.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('item_in.receiving.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $report_status_id = 2;

        $request->validate([
        'name'=>'required',
        'date'=>'required',
        'product.*.id'=>'required|exists:products,product_name',
        'product.*.quantity'=>'required',
        'product.*.quoted_price'=>'required',
      ]);

        $report = new Report([
        'report_name' => $request->get('name'),
        'generated_by' => auth()->user()->id,
        'receive_date' => $request->get('date'),
        'statuses_id' => $report_status_id
      ]);
        $report->save();

        $report_id = DB::select('SELECT id FROM reports ORDER BY id DESC LIMIT 1')[0]->id;
        foreach ($request->get('product') as $key => $value) {
            $item_id = Product::select('id')->
          where('product_name', '=', $value['id'])->
          first();
            $item = new Items([
            'products_id' => $item_id->id,
            'quantity' => $value['quantity'],
            'quoted_price' => $value['quoted_price'],
            'statuses_id' => $report_status_id,
            'report_id' => $report_id
          ]);
            $item->save();
        }

        return redirect('/receiving')->with('success', 'Report Generated!!');
    }

    public function searchProduct(Request $request)
    {
        $result=Product::select('id')->
        where('shelf', 'LIKE', "%{$request->input('query')}%")->
        distinct()->
        limit(10)->
        get();


        $jsonArray = array();

        foreach ($result as $data) {
            $jsonArray[] = json_encode($data->shelf);
        }

        return $jsonArray;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report_items = Items::where('report_id', $id)->sortable()->get();

        return view('item_in.receiving.items', compact('report_items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Items::find($id);
        $prev_status = $item->statuses_id;
        $next_status = 3;
        $item->statuses_id = $next_status;
        $item->save();

        $log = new Log([
          'description' => sprintf('Received the item %s (%s) by %s', $item->product->product_name, $item->quantity, auth()->user()->name),
          'item_id' => $id,
          'from_status_id' => $prev_status,
          'to_status_id' => $next_status,
          'executed_by_id' => auth()->user()->id
        ]);
        $log->save();

        $report_items = Items::where('report_id', $item->report->id)->get();

        return view('item_in.receiving.items', compact('report_items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'report_name'=>'required',
        'generated_by'=>'required',
        'execution_date'=>'required'
      ]);

        $report = Report::find($id);
        $report->report_name = $request->get('report_name');
        $report->generated_by = $request->get('generated_by');
        $report->execution_date = $request->get('execution_date');
        $report->save();

        return redirect('/receiving')->with('success', 'Report updated!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = Report::find($id);
        $report->delete();

        $items = Items::where('report_id', $id)->delete();

        return redirect('receiving')->with('success', 'Report Deleted!!');
    }
}
