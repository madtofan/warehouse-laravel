<?php

namespace App\Http\Controllers\Items;

use App\Report;
use App\Inventory;
use App\Items;
use App\Log;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class PutAwayController extends Controller
{
    /**
    * Get the path the user should be redirected to.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return string
    */
    protected function redirectTo($request)
    {
        return route('login');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $status_id = 3;
        $reports = Report::rightJoin('items', 'reports.id', '=', 'items.report_id')->
    select('reports.id', 'reports.report_name', 'reports.generated_by', 'reports.execution_date')->
    distinct()->
    where('items.statuses_id', '=', $status_id)->
    sortable()->
    paginate(10);

        return view('item_in.putaway.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'select_shelf'=>'required',
        'number-of-item' => 'required',
      ]);

        $add_shelf = $request->get('shelf-list');
        $add_quantity = $request->get('quantity-list');
        $add_shelf[] = $request->get('select_shelf');
        $add_quantity[] = $request->get('number-of-item');

        $item = Items::find($request->get('item'));

        if ($request->get('number-of-item') == $request->get('item-left')) {
            $prev_status = $item->statuses_id;
            $next_status = 4;
            for ($i = 0; $i < count($add_shelf); $i++) {
                $inventory = new Inventory([
              'products_id' => $item->products_id,
              'quantity' => $add_quantity[$i],
              'shelf' => $add_shelf[$i]
          ]);
                $inventory->save();

                $log = new Log([
            'description' => sprintf('Shelved the item %s (%s) to Shelf number %d by %s', $item->product->product_name, $add_quantity[$i], $add_shelf[$i], auth()->user()->name),
            'item_id' => $request->get('item'),
            'from_status_id' => $prev_status,
            'to_status_id' => $next_status,
            'executed_by_id' => auth()->user()->id
          ]);
                $log->save();
            }

            $item->statuses_id = $next_status;
            $item->save();

            return redirect('/putaway/'.$item->report_id)->with('success', 'Item updated!!');
        } else {
            $item_left = $request->get('item-left') - $request->get('number-of-item');

            return view('item_in.putaway.selectshelf', compact('item', 'item_left', 'add_quantity', 'add_shelf'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($report_id)
    {
        $report_items = Items::where('report_id', $report_id)->sortable()->get();

        return view('item_in.putaway.items', compact('report_items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Items::find($id);
        $item_left = $item->quantity;
        $add_shelf = array();
        $add_quantity = array();

        return view('item_in.putaway.selectshelf', compact('item', 'item_left', 'add_shelf', 'add_quantity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
