<?php

namespace App\Http\Controllers\Items;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Report;
use App\Inventory;
use App\Items;
use App\Log;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;

use App\Http\Controllers\Controller;

class PackingController extends Controller
{
    /**
    * Get the path the user should be redirected to.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return string
    */
    protected function redirectTo($request)
    {
        return route('login');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $status_id = 6;
        $reports = Report::rightJoin('items', 'reports.id', '=', 'items.report_id')->
    select('reports.id', 'reports.report_name', 'reports.generated_by', 'reports.execution_date')->
    distinct()->
    where('items.statuses_id', '=', $status_id)->
    sortable()->
    paginate(10);


        return view('item_out.packing.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'select_shelf'=>'required',
      ]);

        $item = Items::find($request->get('item'));
        $inventory = new Inventory([
          'products_id' => $item->products_id,
          'quantity' => $item->quantity,
          'shelf' => $request->get('select_shelf')
      ]);
        $inventory->save();

        $item->statuses_id = 7;
        $item->save();
        $report_items = Items::where('report_id', $item->report_id)->get();

        return redirect('/packing/'.$item->report_id)->with('success', 'Item updated!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($report_id)
    {
        $report_items = Items::where('report_id', $report_id)->sortable()->get();

        return view('item_out.packing.items', compact('report_items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Items::find($id);
        $prev_status = $item->statuses_id;
        $next_status = 7;
        $item->statuses_id = $next_status;
        $item->save();

        $log = new Log([
      'description' => sprintf('Packed the item %s (%s) by %s', $item->product->product_name, $item->quantity, auth()->user()->name),
      'item_id' => $id,
      'from_status_id' => $prev_status,
      'to_status_id' => $next_status,
      'executed_by_id' => auth()->user()->id
    ]);
        $log->save();

        $report_items = Items::where('report_id', $item->report->id)->get();

        return view('item_out.packing.items', compact('report_items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
