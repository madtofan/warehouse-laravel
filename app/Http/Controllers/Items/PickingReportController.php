<?php

namespace App\Http\Controllers\Items;

use App\Product;
use App\Report;
use App\Items;
use App\Inventory;
use App\Log;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Presenter;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class PickingReportController extends Controller
{
    protected function redirectTo($request)
    {
        return route('login');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $status_id = 5;
        $reports = Report::rightJoin('items', 'reports.id', '=', 'items.report_id')->
    select('reports.id', 'reports.report_name', 'reports.generated_by', 'reports.execution_date')->
    distinct()->
    where('items.statuses_id', '=', $status_id)->
    sortable()->
    paginate(10);

        return view('item_out.picking.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item_out.picking.create');
    }

    public function searchProduct(Request $request)
    {
        $result=Product::select('id')->
        where('shelf', 'LIKE', "%{$request->input('query')}")->
        distinct()->
        limit(10)->
        get();

        $jsonArray = array();

        foreach ($result as $data) {
            $jsonArray[] = json_encode($data->shelf);

            return $jsonArray;
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $report_status_id = 5;

        $request->validate([
        'name'=>'required',
        'date'=>'required',
        'product.*.id'=>'required|exists:products,product_name',
        'product.*.quantity'=>'required',
      ]);

        $report = new Report([
        'report_name' => $request->get('name'),
        'generated_by' => auth()->user()->id,
        'receive_date' => $request->get('date'),
        'statuses_id' => $report_status_id
      ]);
        $report->save();

        $report_id = DB::select('SELECT id FROM reports ORDER BY id DESC LIMIT 1')[0]->id;
        foreach ($request->get('product') as $key => $value) {
            $item_id = Product::select('id')->
          where('product_name', '=', $value['id'])->
          first();
            $item = new Items([
            'products_id' => $item_id->id,
            'quantity' => $value['quantity'],
            'quoted_price' => 0,
            'statuses_id' => $report_status_id,
            'report_id' => $report_id
          ]);
            $item->save();
        }

        return redirect('/picking')->with('success', 'Report Generated!!');
    }

    public function pickItem(Request $request)
    {
        $request->validate([
        'select_shelf'=>'required',
        'number-of-item' => 'required',
      ]);

        $add_shelf = $request->get('shelf-list');
        $add_quantity = $request->get('quantity-list');
        $add_shelf[] = $request->get('select_shelf');
        $add_quantity[] = $request->get('number-of-item');

        $item = Items::find($request->get('item'));

        if ($request->get('number-of-item') == $request->get('item-left')) {
            $prev_status = $item->statuses_id;
            $next_status = 6;
            for ($i = 0; $i < count($add_shelf); $i++) {
                $inventory = new Inventory([
              'products_id' => $item->products_id,
              'quantity' => $add_quantity[$i] * -1,
              'shelf' => $add_shelf[$i]
          ]);
                $inventory->save();

                $log = new Log([
            'description' => sprintf('Picked the item %s (%s) from Shelf number %d by %s', $item->product->product_name, $add_quantity[$i], $add_shelf[$i], auth()->user()->name),
            'item_id' => $request->get('item'),
            'from_status_id' => $prev_status,
            'to_status_id' => $next_status,
            'executed_by_id' => auth()->user()->id
          ]);
                $log->save();
            }

            $item->statuses_id = $next_status;
            $item->save();

            return redirect('/picking/'.$item->report_id)->with('success', 'Item updated!!');
        } else {
            $item_left = $request->get('item-left') - $request->get('number-of-item');

            return view('item_out.picking.selectshelf', compact('item', 'item_left', 'add_quantity', 'add_shelf'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report_items = Items::where('report_id', $id)->sortable()->get();

        return view('item_out.picking.items', compact('report_items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Items::find($id);
        $item_left = $item->quantity;
        $add_shelf = array();
        $add_quantity = array();

        return view('item_out.picking.selectshelf', compact('item', 'item_left', 'add_shelf', 'add_quantity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'report_name'=>'required',
        'generated_by'=>'required',
        'execution_date'=>'required'
      ]);

        $report = Report::find($id);
        $report->report_name = $request->get('report_name');
        $report->generated_by = $request->get('generated_by');
        $report->execution_date = $request->get('execution_date');
        $report->save();

        return redirect('/picking')->with('success', 'Report updated!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = Report::find($id);
        $report->delete();

        $items = Items::where('report_id', $id)->delete();

        return redirect('picking')->with('success', 'Report Deleted!!');
    }
}
