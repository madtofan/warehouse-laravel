<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $low_products = DB::select('SELECT p.product_name, p.low_threshold, SUM(i.quantity) quantity FROM products p JOIN inventories i ON p.id = i.products_id GROUP BY product_name, low_threshold HAVING quantity <  p.low_threshold');

        return view('home', compact('low_products'));
    }
    public function admin()
    {
      return view ('admin');
    }
}
