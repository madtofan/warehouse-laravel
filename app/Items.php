<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Items extends Model
{
  use Sortable;

  protected $fillable = [
      'report_id',
      'products_id',
      'quantity',
      'quoted_price',
      'statuses_id'
  ];

  public $sortable = ['report_id', 'products_id', 'quantity', 'quoted_price', 'statuses_id'];

  public function item_nameSortable($query, $direction)
  {
    return $query->leftJoin('products', 'items.products_id', '=', 'products.id')
                    ->orderBy('product_name', $direction)
                    ->select('items.*');
  }

  public function report()
  {
    return $this->belongsTo('App\Report', 'report_id', 'id');
  }

  public function product()
  {
    return $this->belongsTo('App\Product', 'products_id', 'id');
  }

  public function status()
  {
    return $this->belongsTo('App\Status', 'statuses_id', 'id');
  }
}
