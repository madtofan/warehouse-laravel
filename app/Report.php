<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Report extends Model
{
    use Sortable;

    protected $fillable = [
      'report_name',
      'generated_by',
      'execution_date',
      'statuses_id'
  ];

    public $sortable = ['report_name', 'generated_by', 'execution_date', 'statuses_id'];

    public function receiving_items()
    {
        return $this->hasMany('App\Items', 'report_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'generated_by', 'id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status', 'statuses_id', 'id');
    }
}
