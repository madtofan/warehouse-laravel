<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = [
        'products_id',
        'quantity',
        'shelf',
        'created_at'
    ];

    public function product()
    {
      return $this->belongsTo('App\Product', 'products_id', 'id');
    }
}
