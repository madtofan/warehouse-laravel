<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
  protected $fillable = [
      'status_name'
  ];

  public function receiving_items()
  {
    return $this->hasMany('App\Items', 'statuses_id');
  }

  public function report()
  {
    return $this->hasMany('App\Report', 'statuses_id');
  }
}
