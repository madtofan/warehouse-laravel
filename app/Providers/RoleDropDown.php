<?php

namespace App\Providers;
use App\Role;
use Illuminate\Support\ServiceProvider;

class RoleDropDown extends ServiceProvider
{
    public function boot()
    {
      view()->composer('*',function($view){
          $view->with('rolesDD_array', Role::all());
      });
    }
}
