<?php

namespace App\Providers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class ShelfDropDown extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view)
        {
            $view->with('ShelfDD_array', DB::select('SELECT DISTINCT shelf FROM inventories WHERE 1'));
        });
    }
}
