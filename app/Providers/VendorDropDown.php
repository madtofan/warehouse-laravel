<?php

namespace App\Providers;
use App\Vendor;
use Illuminate\Support\ServiceProvider;

class VendorDropDown extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('vendorDD_array', Vendor::all());
        });
    }
}
