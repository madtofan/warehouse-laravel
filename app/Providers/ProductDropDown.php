<?php

namespace App\Providers;
use App\Product;
use Illuminate\Support\ServiceProvider;

class ProductDropDown extends ServiceProvider
{
    public function boot()
    {
      view()->composer('*',function($view){
          $view->with('productsDD_array', Product::all());
      });
    }
}
