<?php

namespace App;

use App\Inventory;
use App\Vendor;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    use Sortable;

    protected $fillable = [
      'product_name',
      'vendors_id',
      'category',
      'price',
      'low_threshold'
  ];

    public $sortable = ['product_name', 'vendors_id', 'category', 'price', 'low_threshold'];

    public function inventories()
    {
        return $this->hasMany('App\Inventory', 'products_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendors_id', 'id');
    }
  
    public static function insertData($data)
    {
        $ignore_count = 0;
        $value=DB::table('products')->where('product_name', $data['product_name'])->get();
        if ($value->count() == 0) {
            DB::table('products')->insert($data);
        } else {
            $ignore_count = 1;
        }

        return $ignore_count;
    }
}
