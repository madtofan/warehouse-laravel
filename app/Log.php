<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Log extends Model
{
  use Sortable;

  protected $fillable = [
      'description',
      'item_id',
      'from_status_id',
      'to_status_id',
      'executed_by_id',
  ];

  public $sortable = ['description', 'item_id', 'executed_by_id', 'created_at'];

  public function item()
  {
    return $this->belongsTo('App\Items', 'item_id', 'id');
  }

  public function from_status()
  {
    return $this->belongsTo('App\Status', 'from_status_id', 'id');
  }

  public function to_status()
  {
    return $this->belongsTo('App\Status', 'to_status_id', 'id');
  }

  public function executed_by()
  {
    return $this->belongsTo('App\User', 'executed_by_id', 'id');
  }
}
