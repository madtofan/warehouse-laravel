<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Role extends Model
{
  Use Sortable;

  protected $fillable = [
      'role_name',
      'admin',
      'moderator',
      'receiving',
      'inventory',
      'putting_away'
  ];

  public $sortable = ['role_name', 'admin', 'moderator', 'receiving', 'inventory', 'putting_away'];

  public function users()
  {
    return $this->hasMany('App\User', 'roles_id');
  }
}
