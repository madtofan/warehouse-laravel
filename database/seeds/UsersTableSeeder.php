<?php

Use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create('ms_MY');
        $faker->addProvider(new Faker\Provider\Internet($faker)); //for $faker password

        User::create(['name' => 'Ahmad',
                'email' => 'ahmadclab@gmail.com',
                'password' => bcrypt('Password1'),
                'roles_id' => 1]);

        User::create(['name' => 'Muhammad Nizam',
                'email' => 'muhdnijam@gmail.com',
                'password' => bcrypt('Password1'),
                'roles_id' => 1]);

        User::create(['name' => 'Afiq Irfan',
                'email' => 'afiqirfan08@gmail.com',
                'password' => bcrypt('Password1'),
                'roles_id' => 1]);

        User::create(['name' => 'Guest Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('Password1'),
                'roles_id' => 1]);

        for($i = 0; $i<100; $i++){
        User::create([
                    'name' => $faker->name,
                    'email' => $faker->email,
                    'password' => bcrypt('Password1'),
                    'roles_id' => rand(1,100),
                ]);
            }

    }
}
