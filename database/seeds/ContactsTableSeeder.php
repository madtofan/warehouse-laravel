<?php

Use App\Contact;
use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create('ms_MY');
        $faker->addProvider(new Faker\Provider\en_US\Company($faker)); //for $faker job title

        for($i = 0; $i<1000; $i++){
        Contact::create([
                'name' => $faker->name,
                'vendors_id' => rand(1,100),
                'email' => $faker->freeEmail,
                'job_title' => $faker->jobTitle('en_US'),
                'city' => $faker->state,
                'phone_number' => $faker->mobileNumber($countryCodePrefix = null|true|false, $formatting = null|true|false),
        ]);
        }

    }

}
