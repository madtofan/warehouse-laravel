<?php

use App\Inventory;
use Illuminate\Database\Seeder;

class InventoriesTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i<1000; $i++){
        App\Inventory::create([
                'products_id' => rand(1,100),
                'quantity' => rand(1,100),
                'shelf' => rand(1,100),
        ]);
        }
    }
}

