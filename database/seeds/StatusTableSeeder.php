<?php

Use App\Status;
use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder {

    public function run()
    {
        Status::create(['status_name' => 'Approval']);
        Status::create(['status_name' => 'Receving']);
        Status::create(['status_name' => 'Put Away']);
        Status::create(['status_name' => 'Inventory']);
        Status::create(['status_name' => 'Picking']);
        Status::create(['status_name' => 'Packing']);
        Status::create(['status_name' => 'Sent']);
    }
}
