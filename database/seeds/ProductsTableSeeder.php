<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create('ms_MY');
        \Bezhanov\Faker\ProviderCollectionHelper::addAllProvidersTo($faker); //for $faker product name && department

        for($i = 0; $i<1000; $i++){
        Product::create([
                'product_name' => $faker->productName.' ver.'.rand(1,999),
                'vendors_id' => rand(1,100),
                'category' => $faker->department,
                'low_threshold' => rand(1,10),
                'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 999),
        ]);
        }
    }
}
