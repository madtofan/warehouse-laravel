<?php

Use App\Report;
Use App\Items;
use Illuminate\Database\Seeder;

class ReceivingReportTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create('ms_MY');
        $faker->addProvider(new Faker\Provider\Base($faker)); // for $faker randomElements

        for($report = 1; $report<1000; $report++){
            $status = rand(1, 7);
            Report::create([
                'report_name' => $faker->randomElement($array = array ('First Receiving Report','Second Receiving Report','First Put Away Report', 'Second Put Away Report')),
                'generated_by' => rand(1,100),
                'execution_date' => $faker->dateTime($max = 'now'),
                'statuses_id' => $status,
            ]);

            for($item = 0; $item<rand(5,20); $item++){
                Items::create([
                    'report_id' => $report,
                    'products_id' => rand(1,100),
                    'quantity' => rand(1,100),
                    'quoted_price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 999),
                    'statuses_id' => $status,
                ]);
            }
        }
    }
}
