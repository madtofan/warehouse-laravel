<?php

Use App\Vendor;
use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create('ms_MY');

        for($i = 0; $i<1000; $i++){
        Vendor::create([
                'vendor_name' => $faker->companyName,
                'vendor_address' => $faker->address,
                'vendor_email' => $faker->email,
                'vendor_number' => $faker->fixedLineNumber($countryCodePrefix = null|true|false, $formatting = null|true|false),
        ]);
        }
    }
}
