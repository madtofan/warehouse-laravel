<?php

Use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create('ms_MY');
        $faker->addProvider(new Faker\Provider\Miscellaneous($faker)); //for $faker boolean

        Role::create([
                'role_name' => 'Admin',
                'admin' => TRUE,
                'moderator' => TRUE,
                'receiving' => TRUE,
                'inventory' => TRUE,
                'putting_away' => TRUE,
            ]);

        Role::create([
                'role_name' => 'No Admin',
                'admin' => FALSE,
                'moderator' => TRUE,
                'receiving' => TRUE,
                'inventory' => TRUE,
                'putting_away' => TRUE,
              ]);

        for($i = 0; $i<1000; $i++){
        Role::create([
                'role_name' => $faker->randomElement($array = array ('Admin')),
                'admin' => $faker->boolean,
                'moderator' => $faker->boolean,
                'receiving' => $faker->boolean,
                'inventory' => $faker->boolean,
                'putting_away' => $faker->boolean,
            ]);
        }
    }
}
