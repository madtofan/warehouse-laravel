<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->command->info('Roles table seeded!');

        $this->call(StatusTableSeeder::class);
        $this->command->info('Status table seeded!');

        $this->call(UsersTableSeeder::class);
        $this->command->info('Users table seeded!');

        $this->call(VendorsTableSeeder::class);
        $this->command->info('Vendors table seeded!');

        $this->call(ContactsTableSeeder::class);
        $this->command->info('Contacts table seeded!');

        $this->call(ProductsTableSeeder::class);
        $this->command->info('Products table seeded!');

        $this->call(InventoriesTableSeeder::class);
        $this->command->info('Inventory table seeded!');

        $this->call(ReceivingReportTableSeeder::class);
        $this->command->info('Receiving Report table seeded!');
    }
}
