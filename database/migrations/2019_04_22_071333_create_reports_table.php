<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('report_name');
            $table->integer('generated_by')->unsigned();
            $table->timestamp('execution_date');
            $table->integer('statuses_id')->unsigned();
            $table->timestamps();

            $table->foreign('generated_by')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->foreign('statuses_id')
                  ->references('id')
                  ->on('statuses')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
