<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->integer('item_id')->unsigned();
            $table->integer('from_status_id')->unsigned();
            $table->integer('to_status_id')->unsigned();
            $table->integer('executed_by_id')->unsigned();
            $table->timestamps();

            $table->foreign('item_id')
                  ->references('id')
                  ->on('items')
                  ->onDelete('cascade');

            $table->foreign('from_status_id')
                  ->references('id')
                  ->on('statuses')
                  ->onDelete('cascade');

            $table->foreign('to_status_id')
                  ->references('id')
                  ->on('statuses')
                  ->onDelete('cascade');

            $table->foreign('executed_by_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
