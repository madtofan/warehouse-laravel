<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main Page
Route::get('/', 'HomeController@index')->name('home');
Auth::routes(['request' => false, 'reset' => false]);

// Inventory Management
Route::resource('/summary', 'Inventory\SummaryController');
Route::get('/summary/filter/{filter}', 'Inventory\SummaryController@filter');
Route::get('/history', 'Inventory\SummaryController@history')->name('inventory');
Route::resource('/list_shelf', 'Inventory\RecheckShelfController');
Route::resource('/list_product', 'Inventory\ProductListController');
Route::get('searchShelf', 'Inventory\RecheckShelfController@searchShelf');
Route::get('searchProduct', 'Inventory\ProductListController@searchProduct');
// Receiving / Put Away
Route::resource('/receiving', 'Items\ReceivingReportController');
Route::resource('/putaway', 'Items\PutAwayController');

// Picking / Packing
Route::resource('/picking', 'Items\PickingReportController');
Route::post('/pick_item', 'Items\PickingReportController@pickItem');
Route::resource('/packing', 'Items\PackingController');

// Administration
Route::resource('/contacts', 'Administration\ContactController');
Route::get('/contacts/filter/{filter}', 'Administration\ContactController@filter');
Route::resource('/products', 'Administration\ProductController');
Route::get('/products/filter/{filter}', 'Administration\ProductController@filter');
Route::resource('/logs', 'Administration\LogController');
Route::post('/report-download', 'Administration\LogController@report');
Route::resource('/roles', 'Administration\RoleController');
Route::get('/roles/filter/{filter}', 'Administration\RoleController@filter');
Route::resource('/users', 'Administration\UserController');
Route::get('/users/filter/{filter}', 'Administration\UserController@filter');
Route::resource('/vendors', 'Administration\VendorController');
Route::get('/vendors/filter/{filter}', 'Administration\VendorController@filter');
Route::post('/uploadproductfile', 'Administration\ProductController@uploadFile');
Route::post('/uploadvendorfile', 'Administration\VendorController@uploadFile');

// Download Route
Route::get('download/{filename}', function ($filename) {
    // Check if file exists in app/storage/file folder
    $file_path = storage_path() .'/file/'. $filename;
    if (file_exists($file_path)) {
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    } else {
        // Error
        exit('Requested file does not exist on our server!');
    }
})
->where('filename', '[A-Za-z0-9\-\_\.]+');
